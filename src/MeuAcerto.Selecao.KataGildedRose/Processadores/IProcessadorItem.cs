﻿namespace MeuAcerto.Selecao.KataGildedRose.Processadores
{
    public interface IProcessadorItem
    {
        void AtualizarQualidade(Item item);
    }
}
