﻿using System;

namespace MeuAcerto.Selecao.KataGildedRose.Processadores
{
    public class ProcessadorItemConjurado : IProcessadorItem
    {
        public void AtualizarQualidade(Item item)
        {
            if (item is null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            item.DiminuirQualidade(2);
            item.PrazoParaVenda -= 1;
        }
    }
}
