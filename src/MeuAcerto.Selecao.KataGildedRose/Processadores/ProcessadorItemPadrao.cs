﻿using System;

namespace MeuAcerto.Selecao.KataGildedRose.Processadores
{
    public class ProcessadorItemPadrao : IProcessadorItem
    {
        public void AtualizarQualidade(Item item)
        {
            if (item is null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            item.DiminuirQualidade(1);
            item.PrazoParaVenda -= 1;

            if (item.PrazoParaVenda < 0)
            {
                item.DiminuirQualidade(1);
            }
        }
    }
}
