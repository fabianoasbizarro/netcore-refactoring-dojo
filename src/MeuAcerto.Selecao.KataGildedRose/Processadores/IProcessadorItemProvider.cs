﻿namespace MeuAcerto.Selecao.KataGildedRose.Processadores
{
    public interface IProcessadorItemProvider
    {
        IProcessadorItem GetProcessadorItem(Item item);
    }

    public class ProcessadorItemProvider : IProcessadorItemProvider
    {
        public IProcessadorItem GetProcessadorItem(Item item) => item.Nome switch
        {
            "Bolo de Mana Conjurado" => new ProcessadorItemConjurado(),
            "Dente do Tarrasque" => new ProcessadorItemLendario(),
            "Ingressos para o concerto do Turisas" => new ProcessadorItemIngresso(),
            "Queijo Brie Envelhecido" => new ProcessadorItemQueijo(),
            _ => new ProcessadorItemPadrao(),
        };
    }
}
