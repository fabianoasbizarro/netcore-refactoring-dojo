﻿using System;

namespace MeuAcerto.Selecao.KataGildedRose.Processadores
{
    public class ProcessadorItemIngresso : IProcessadorItem
    {
        public void AtualizarQualidade(Item item)
        {
            if (item is null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            item.AumentarQualidade(1);

            if (item.PrazoParaVenda <= 10)
            {
                item.AumentarQualidade(1);
            }
            
            if (item.PrazoParaVenda <= 5)
            {
                item.AumentarQualidade(1);
            }

            item.PrazoParaVenda -= 1;

            if (item.PrazoParaVenda < 0)
            {
                item.Qualidade = 0;
            }
        }
    }
}
