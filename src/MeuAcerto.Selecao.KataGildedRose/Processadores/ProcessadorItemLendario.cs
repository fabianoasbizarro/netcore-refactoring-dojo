﻿using System;

namespace MeuAcerto.Selecao.KataGildedRose.Processadores
{
    public class ProcessadorItemLendario : IProcessadorItem
    {
        public void AtualizarQualidade(Item item)
        {
            if (item is null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            if (item.Qualidade != 80)
                item.Qualidade = 80;
        }
    }
}
