﻿using MeuAcerto.Selecao.KataGildedRose.Processadores;
using System;
using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        private readonly IProcessadorItemProvider _provider;

        IList<Item> Itens;

        public GildedRose(IList<Item> Itens)
            : this(Itens, new ProcessadorItemProvider())
        {
        }

        public GildedRose(IList<Item> itens, IProcessadorItemProvider provider)
        {
            _provider = provider ?? throw new ArgumentNullException(nameof(provider));
            Itens = itens ?? throw new ArgumentNullException(nameof(itens));
        }

        public void AtualizarQualidade()
        {
            for (int i = 0; i < Itens.Count; i++)
            {
                var item = Itens[i];
                var processadorItem = _provider.GetProcessadorItem(item);
                processadorItem.AtualizarQualidade(item);
            }
        }
    }
}
