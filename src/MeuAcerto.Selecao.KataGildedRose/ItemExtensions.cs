﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public static class ItemExtensions
    {
        public static void AumentarQualidade(this Item item, int valor = 1)
        {
            if (item.Qualidade < 50)
            {
                if (item.Qualidade + valor > 50)
                {
                    item.Qualidade = 50;
                }
                else
                {
                    item.Qualidade += valor;
                }
            }
        }

        public static void DiminuirQualidade(this Item item, int valor = 1)
        {
            if (item.Qualidade > 0)
            {
                if (item.Qualidade - valor < 0)
                {
                    item.Qualidade = 0;
                }
                else
                {
                    item.Qualidade -= valor;
                }
            }
        }
    }
}
